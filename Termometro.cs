
namespace Laboratorio9
{
    public class Termometro
    {
        public double Temperatura{ get; set; }
        public Termometro()
        {
            Temperatura = 0.0;
        }

        virtual public void Aumentar()
        {
            Temperatura += 0.1;
        }
        virtual public void Aumentar(double quantia)
        {
            Temperatura += quantia;
        }
        virtual public void Diminuir()
        {
            Temperatura -= 0.1;
        }
        virtual public void Diminuir(double quantia)
        {
            Temperatura -= quantia;
        }
        public override string ToString()
        {
            return Temperatura.ToString();
        }

    }
}