﻿using System;

namespace Laboratorio9
{
    class Program
    {
        static void Main(string[] args)
        {
            TermometroLimite term = new TermometroLimite(5);
            Console.WriteLine(term.ToString());

            term.LimiteSuperiorEvent += new TermometroLimite.MeuDelegate(TrataLimiteSuperior);

            term.Aumentar(6);
            Console.WriteLine(term.ToString());

            term.Diminuir(2);
            Console.WriteLine(term.ToString());

            term.Aumentar(1);
            Console.WriteLine(term.ToString());

            term.Aumentar(1);
            Console.WriteLine(term.ToString());

            term.Diminuir(1);
            Console.WriteLine(term.ToString());

            term.Diminuir(2);
            Console.WriteLine(term.ToString());

            term.Diminuir(1);
            Console.WriteLine(term.ToString());


        }

        private static void TrataLimiteSuperior(string msg)
        {
            Console.WriteLine(msg);
        }
    }
}
