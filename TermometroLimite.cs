using System;

namespace Laboratorio9
{
    public class TermometroLimite : Termometro
    {
        public double LimiteSuperior{ get; set; }
        private bool disparadoEventoLimiteSuperior;
        public delegate void MeuDelegate( string msg );
        public event MeuDelegate LimiteSuperiorEvent;

        public TermometroLimite(double ls)
        {
            LimiteSuperior = ls;
            disparadoEventoLimiteSuperior = false;
        }

        private void OnLimiteSuperiorEvent()
        {
            if((this.Temperatura > LimiteSuperior) && (!disparadoEventoLimiteSuperior))
            {
                if (LimiteSuperiorEvent != null)
                {
                    LimiteSuperiorEvent($"Atenção: temperatura  acima do limite !!!\nTemperatura atual= {this.Temperatura}° ");
                    disparadoEventoLimiteSuperior = true;
                }
            }
        }

        private void OffLimiteSuperiorEvent()
        {
            if((this.Temperatura <= LimiteSuperior) && (disparadoEventoLimiteSuperior))
            {
                if (LimiteSuperiorEvent != null)
                {
                    LimiteSuperiorEvent($"Temperatura normalizada.\nTemperatura atual= {this.Temperatura}°");
                    disparadoEventoLimiteSuperior = false;
                }
            }
        }

        public override void Aumentar()
        {
            base.Aumentar();
            OnLimiteSuperiorEvent();
        }

         public override void Aumentar(double quantia)
        {
            base.Aumentar(quantia);
            OnLimiteSuperiorEvent();
        }

        public override void Diminuir()
        {
            base.Diminuir();
            OffLimiteSuperiorEvent();
        }

        public override void Diminuir(double quantia)
        {
            base.Diminuir(quantia);
            OffLimiteSuperiorEvent();
        }

    }
}